import { createAction, props } from '@ngrx/store';

/**
 * Effects
*/

/**
 * Basic Actions
*/

export const mainSetCount = createAction(
  '[Main] Set Count',
  props<{ payload: number }>()
);
