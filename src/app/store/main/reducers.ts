import { createReducer, on } from '@ngrx/store';

import * as actions from './actions';

export const initialState: any = {
  count: 0,
};

const _mainReducer = createReducer(initialState,
  on(actions.mainSetCount, (state, { payload }) => {
    return { ...state, count: payload };
  }),
);

export function reducer(state, action) {
  return _mainReducer(state, action);
}
