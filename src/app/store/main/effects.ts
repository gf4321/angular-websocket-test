import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, tap, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as actions from './actions';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { Store } from '@ngrx/store';

@Injectable()
export class TestEffects {
  constructor(
    private actions$: Actions,
    private webSocketService: WebSocketService,
    private store: Store<any>
    ) {}

  /**
   * With this technique, we trigger an effect to establish
   * the websocket connection. Since the connection observable will then
   * continue to emit values over time (as the server continues to send messages
   * back to us), we map each of these messages to actions as they arrive.
   * So these actions will continue to be dispatched until we disconnect from the
   * websocket. (But keep in mind that we also retry when we disconnect)
  */
  // testConnectWebSocket$ = createEffect(
  //   () => this.actions$.pipe(
  //     ofType(actions.testConnectWebSocket),
  //     tap((a) => {
  //       // console.log('effect firing', a);
  //       this.store.dispatch(actions.testWebSocketConnected())
  //     }),
  //     switchMap(({ url }) => this.webSocketService.connect(url).pipe(
  //       switchMap((res) => {
  //         console.log('testConnectWebSocket res', res);
  //         return of(
  //           actions.testSetSocketValues({ payload: res }),
  //         )
  //       }),
  //       catchError((err) => {
  //         console.log('err', err);
  //         return of(actions.testSetSocketError({ payload: err }));
  //       }),
  //     ))
  //   )
  // );
}
